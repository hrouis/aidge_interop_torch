import torch
import aidge_interop_torch
import aidge_core
import unittest

import numpy as np

torch.backends.cudnn.enabled = True
torch.backends.cudnn.benchmark = True
weight_value = 0.05
batch_size = 10
learning_rate = 0.01
comparison_precision = 0.001
absolute_presision = 0.0001
epochs = 10

# TODO : add tensor test later ...

# class test_tensor_conversion(unittest.TestCase):

#     def setUp(self):
#         self.batch_size = 1
#         self.channel = 2
#         self.x = 3
#         self.y = 4

#     def test_torch_to_aidge(self):
#         torch_tensor = torch.ones(
#             self.batch_size, self.channel, self.y, self.x)
#         n2d2_tensor = aidge_interop_torch.convert_tensor(torch_tensor)
#         # self.assertEqual(n2d2_tensor.dims(), self.n2d2_format)

#     def test_aidge_to_torch(self):
#         n2d2_tensor = aidge_core.Tensor(np.ones((self.batch_size, self.channel, self.y, self.x)))
#         torch_tensor = aidge_interop_torch.convert_tensor(n2d2_tensor)
#         # self.assertEqual(list(torch_tensor.shape), self.torch_format)


# DEFINE A TESTER CLASS
class Test_Networks():
    """
    A custom class to automate the test of two networks
    """

    def __init__(self, model1, model2, name="", test_backward=True, eval_mode=False, epochs=10, relative_precision=0.001, absolute_presision=0.0001, learning_rate=0.01, cuda=False):
        self.relative_precision = relative_precision
        self.absolute_presision = absolute_presision
        self.epochs = epochs
        self.test_backward = test_backward
        self.model1 = model1
        self.model2 = model2
        self.cuda = cuda
        if self.cuda:
            self.model1 = self.model1.cuda()
            self.model2 = self.model2.cuda()
        if eval_mode:
            self.model1.eval()
            self.model2.eval()
        else:
            self.model1.train()
            self.model2.train()
        self.name = name

        if self.test_backward:
            self.optimizer1 = torch.optim.SGD(
                self.model1.parameters(), lr=learning_rate)
            self.optimizer2 = torch.optim.SGD(
                self.model2.parameters(), lr=learning_rate)
            self.criterion1 = torch.nn.MSELoss()
            self.criterion2 = torch.nn.MSELoss()

    def compare_tensor(self, t1, t2):
        for i, j in zip(torch.flatten(t1), torch.flatten(t2)):
            i = i.item()
            j = j.item()
            if j != 0:
                if abs(i-j) > self.relative_precision * abs(j) + self.absolute_presision:
                    return -1
        return 0

    def unit_test(self, input_tensor, label):
        torch_tensor1 = input_tensor
        torch_tensor2 = input_tensor.detach().clone()
        print(torch_tensor1)
        if self.test_backward:
            label1 = label
            label2 = label.detach().clone()
        output1 = self.model1(torch_tensor1)
        output2 = self.model2(torch_tensor2)

        if self.compare_tensor(output1, output2) != 0:
            print("The test " + self.name +
                  " failed, the following output tensor are different :\nOutput 1")
            print(output1)
            print("Output 2")
            print(output2)
            return -1

        if self.test_backward:
            loss1 = self.criterion1(output1, label1)
            self.optimizer1.zero_grad()
            loss1.backward()
            self.optimizer1.step()

            loss2 = self.criterion2(output2, label2)
            self.optimizer2.zero_grad()
            loss2.backward()
            self.optimizer2.step()
            if self.compare_tensor(loss1, loss2):
                print("Different loss : ", loss1.item(), "|", loss2.item())
                return -1
        return 0

    def test_multiple_step(self, input_size, label_size):
        for i in range(self.epochs):
            input_tensor = torch.randn(input_size)
            if self.test_backward:
                label_tensor = torch.ones(label_size)
            else:
                label_tensor = None
            if self.cuda:
                input_tensor = input_tensor.cuda()
                if self.test_backward:
                    label_tensor = label_tensor.cuda()
            if self.unit_test(input_tensor, label_tensor):
                print("Difference occurred on Epoch :", i)
                return -1
        return 0


class TorchLeNet(torch.nn.Module):

    def __init__(self):
        super(TorchLeNet, self).__init__()
        c1 = torch.nn.Conv2d(1, 6, 5, bias=False)
        c2 = torch.nn.Conv2d(6, 16, 5, bias=False)
        c3 = torch.nn.Conv2d(16, 120, 5, bias=False)
        l1 = torch.nn.Linear(120, 84)
        l2 = torch.nn.Linear(84, 10)

        torch.nn.init.constant_(c1.weight, weight_value)
        torch.nn.init.constant_(c2.weight, weight_value)
        torch.nn.init.constant_(c3.weight, weight_value)
        torch.nn.init.constant_(l1.weight, weight_value)
        torch.nn.init.constant_(l2.weight, weight_value)

        self.layer = torch.nn.Sequential(
            c1,
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(6),
            torch.nn.ReLU(),
            torch.nn.MaxPool2d(2),
            c2,
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(16),
            torch.nn.ReLU(),
            torch.nn.MaxPool2d(2),
            c3,
            torch.nn.ReLU(),
            torch.nn.Flatten(),
            l1,
            torch.nn.ReLU(),
            l2,
        )
        self.sequence = torch.nn.Sequential(
            self.layer,
        )

    def forward(self, x):
        x = self.sequence(x)
        return x


class test_interop(unittest.TestCase):

    def tearDown(self):
        pass

    def test_LeNet_CPU(self):
        print('=== Testing LENET CPU ===')
        input_size = (batch_size, 1, 32, 32)
        torch_model = TorchLeNet()
        aidge_model = aidge_interop_torch.wrap(torch_model, input_size)

        # TODO: add test for train
        # print("Train ...")
        # tester = Test_Networks(torch_model, aidge_model, eval_mode=False, epochs=epochs)
        # res = tester.test_multiple_step(input_size, (batch_size, 10))
        # self.assertNotEqual(res, -1, msg="CPU train failed")
        print("Eval ...")
        tester = Test_Networks(
            torch_model, aidge_model, eval_mode=True, epochs=epochs, test_backward=False)
        res = tester.test_multiple_step(input_size, (batch_size, 10))
        self.assertNotEqual(res, -1, msg="LeNet CPU eval failed")


if __name__ == '__main__':
    unittest.main()
