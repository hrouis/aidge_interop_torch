import torch
import numpy as np
import onnx

import aidge_core
import aidge_backend_cpu

import aidge_onnx

from onnxsim import simplify
from typing import Union


def convert_tensor(tensor):
    if isinstance(tensor, torch.Tensor):
        return torch_tensor_to_aidge(tensor)
    elif isinstance(tensor, aidge_core.Tensor):
        return aidge_tensor_to_torch(tensor)
    else:
        raise RuntimeError(
            f"Object of type {type(tensor)} is not convertible.")


def aidge_tensor_to_torch(aidge_tensor):
    """
    Convert Aidge.Tensor -> torch.Tensor
    The conversion creates a GPU memory copy if the tensor is CUDA.
    This method also convert the shape of the tensor to follow torch convention.
    """
    torch_tensor = torch.from_numpy(np.array(aidge_tensor))
    # TODO : handle cuda case !
    # if aidge_tensor.is_cuda:
    #     torch_tensor = torch_tensor.cuda() # Create GPU memory copy
    return torch_tensor


def torch_tensor_to_aidge(torch_tensor):
    aidge_tensor = None
    numpy_tensor = torch_tensor.cpu().detach().numpy()
    # This operation creates a CPU memory copy.
    # torch.Tensor can have a discontiguous memory while aidge.Tensor need a contiguous memory space.
    aidge_tensor = aidge_core.Tensor(numpy_tensor)
    # if aidge_tensor.nb_dims() == 4:
    #     aidge_tensor.reshape(_switching_convention(aidge_tensor.dims()))
    return aidge_tensor


class AidgeModule(torch.nn.Module):
    """
    PyTorch layer used to interface an :py:class:`aidge_core.GraphView` object in a PyTorch Network.
    """
    _initialized = False

    def __init__(self, graph_view, batch_size=None):
        """
        :param block: Aidge block object to interface with PyTorch
        :type block: :py:class:`aidge_core.GraphView`
        """
        print("TEST")
        super().__init__()
        if not isinstance(graph_view, aidge_core.GraphView):
            raise TypeError(
                "Parameter graph_view should be of type aidge_core.GraphView got " + str(type(graph_view)) + " instead")
        self._graph_view = graph_view

        # TODO : better handling of backend ?
        # maybe a function set_backend similar to PyTorch ?
        print("Set cpu !")
        graph_view.set_backend("cpu")

        # We need to add a random parameter to the module else pytorch refuse to compute gradient
        self.register_parameter(name='random_parameter',
                                param=torch.nn.Parameter(torch.ones(1)))

        self.batch_size = batch_size  # Batch size used to define the neural network
        self.current_batch_size = None
        self.multi_outputs_flag = False
        # TODO support of multi input graph later ...
        self.input_nodes = [None]
        self.scheduler = None

    def forward(self, inputs: torch.Tensor):
        """
        Use a custom ``torch.autograd.Function`` that use, on forward the ``Propagation`` of Aidge.
        And on backward the ``BackPropagation`` and ``Update`` of Aidge.
        """
        class graph_view_computation(torch.autograd.Function):
            """
            Autograd function that will use the propagation/backpropagation/update of Aidge.
            """
            @staticmethod
            def forward(ctx, inputs):
                aidge_tensor = torch_tensor_to_aidge(inputs)

                # TODO: add a system to avoid creating a new node everytime

                if self.input_nodes[0] == None:
                    self.input_nodes[0] = aidge_core.Producer(
                        aidge_tensor, "Input_0")

                    # TODO: get datatype & backend from graph view
                    self.input_nodes[0].get_operator().set_datatype(
                        aidge_core.DataType.Float32)
                    self.input_nodes[0].get_operator().set_backend("cpu")

                    self.input_nodes[0].add_child(self._graph_view)
                    self._graph_view.add(self.input_nodes[0])
                else:
                    self.input_nodes[0].get_operator(
                    ).set_output(0, aidge_tensor)
                # TODO: create one scheduler ! maybe in init ?

                if not self.scheduler:
                    self.scheduler = aidge_core.SequentialScheduler(
                        self._graph_view)

                # Run inference !
                self.scheduler.forward(verbose=True)

                # TODO: support for multi output later ?
                if len(self._graph_view.get_output_nodes()) > 1:
                    raise RuntimeError(
                        "Torch interop only support graph with one output.")
                aidge_output = list(self._graph_view.get_output_nodes())[
                    0].get_operator().get_output(0)
                torch_output = aidge_tensor_to_torch(aidge_output)
                return torch_output.clone()

            @staticmethod
            def backward(ctx, grad_output):
                raise RuntimeError("Backward is not yet supported with Aidge.")

        # If the layer is at the beginning of the network requires grad is False.
        inputs.requires_grad = True

        outputs = graph_view_computation.apply(inputs)
        return outputs


class ContextNoBatchNormFuse:
    """
    PyTorch fuse batchnorm if train = False.
    If train = True batchnorm stats are updated with the dummy tensor and are invalidated.
    Solution : Temporary replacement of the forward method for every batchnorm.
    """

    def __init__(self, model):
        self.model = model
        self.fowards = []

    def __enter__(self):
        """Change batchnorm forward behavior when entering the block.
        """
        for module in self.model.modules():
            if isinstance(module, torch.nn.modules.batchnorm._BatchNorm):
                def fake_forward(inputs,
                                 current_bn=module):
                    """New batchnorm forward which save statistics and restore them after propagation
                    """
                    if current_bn.momentum is None:
                        eaf = 0.0
                    else:
                        eaf = current_bn.momentum
                    assert current_bn.num_batches_tracked is not None
                    current_bn.num_batches_tracked.add_(1)
                    if current_bn.momentum is None:  # use cumulative moving average
                        eaf = 1.0 / current_bn.num_batches_tracked.item()
                    else:  # use exponential moving average
                        eaf = current_bn.momentum

                    # Save copy of values before propagation
                    saved_run_mean = current_bn.running_mean.detach().clone()
                    saved_run_var = current_bn.running_var.detach().clone()
                    if current_bn.affine:  # Bias and Weights only created if affine=True
                        saved_bias = current_bn.bias.detach().clone()
                        saved_weight = current_bn.weight.detach().clone()
                    print(current_bn.running_mean.shape)
                    # Real batchnorm forward
                    output_tensor = torch.nn.functional.batch_norm(
                        inputs,
                        current_bn.running_mean,  # running_mean
                        current_bn.running_var,  # running_var
                        current_bn.weight,       # weight
                        current_bn.bias,         # bias
                        True,                    # bn training
                        eaf,                     # exponential_average_factor
                        current_bn.eps,          # epsilon
                    )

                    current_bn.running_mean.copy_(torch.nn.Parameter(
                        saved_run_mean).requires_grad_(False))
                    current_bn.running_var.copy_(torch.nn.Parameter(
                        saved_run_var).requires_grad_(False))
                    if current_bn.affine:
                        current_bn.bias = (torch.nn.Parameter(saved_bias))
                        current_bn.weight = (torch.nn.Parameter(saved_weight))

                    return output_tensor
                # Update Batchnorm forward with the fake forward !
                self.fowards.append(module.forward)
                module.forward = fake_forward
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Restore batchnorm forward behavior when exiting the block.
        """
        cpt = 0
        for module in self.model.modules():
            if isinstance(module, torch.nn.modules.batchnorm._BatchNorm):
                # Restore Batchnorm forward
                # torch.nn.modules.batchnorm._BatchNorm.forward
                module.forward = self.fowards[cpt]
                cpt += 1


def wrap(torch_model: torch.nn.Module,
         input_size: Union[list, tuple],
         opset_version: int = 11,
         in_names: list = None,
         out_names: list = None,
         verbose: bool = False) -> AidgeModule:
    """Function generating a ``torch.nn.Module`` which embed a :py:class:`aidge_core.GraphView`.
    The torch_model is exported to Aidge via ONNX.

    :param torch_model: Torch model to convert to Aidge.
    :type torch_model: ``torch.nn.Module``
    :param input_size: The size of the input of the network, the format required is NCHW.
    :type input_size: ``list``
    :param opset_version: Opset version used to generate the intermediate ONNX file, default=11
    :type opset_version: int, optional
    :param in_names: Specify specific names for the network inputs
    :type in_names: list, optional
    :param out_names: Specify specific names for the network outputs
    :type in_names: list, optional
    :param verbose: Enable the verbose output of torch onnx export, default=False
    :type verbose: bool, optional
    :return: A custom ``torch.nn.Module`` which embed a :py:class:`aidge_core.GraphView`.
    :rtype: :py:class:`aidge_interop_torch.AidgeModule`
    """
    raw_model_path = f'./{torch_model.__class__.__name__}_raw.onnx'
    model_path = f'./{torch_model.__class__.__name__}.onnx'
    print("Exporting torch module to ONNX ...")

    try:
        torch_device = next(torch_model.parameters()).device
    except StopIteration:
        torch_device = torch.device('cpu') # Model has no parameter, defaulting to cpu

    dummy_in = torch.zeros(input_size).to(torch_device)

    # Note : To keep batchnorm we export model in train mode.
    # However we cannot freeze batchnorm stats in pytorch < 12 (see : https://github.com/pytorch/pytorch/issues/75252).
    # And even in > 12 when stats freezed the ONNX graph drastically changes ...
    # To deal with this issue we use a context which change the forward behavior of batchnorm to protect stats.
    with ContextNoBatchNormFuse(torch_model) as ctx:
        torch.onnx.export(torch_model,
                          dummy_in,
                          raw_model_path,
                          verbose=verbose,
                          input_names=in_names,
                          output_names=out_names,
                          export_params=True,
                          opset_version=opset_version,
                          training=torch.onnx.TrainingMode.TRAINING,
                          do_constant_folding=False)

    print("Simplifying the ONNX model ...")
    onnx_model = onnx.load(raw_model_path)
    model_simp, check = simplify(onnx_model)
    assert check, "Simplified ONNX model could not be validated"
    onnx.save(model_simp, model_path)
    aidge_model = aidge_onnx.onnx_import._load_onnx2graphview(model_simp)

    aidge_core.remove_flatten(aidge_model)
    return AidgeModule(aidge_model)
