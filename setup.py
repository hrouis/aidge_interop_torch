#!/usr/bin/env python3
""" Aidge

#TODO To change
POC of the next framework named Aidge
"""

DOCLINES = (__doc__ or '').split("\n")

import sys
import os

# Python supported version checks
if sys.version_info[:2] < (3, 7):
    raise RuntimeError("Python version >= 3.7 required.")


CLASSIFIERS = """\
Development Status :: 2 - Pre-Alpha
"""

import shutil
import pathlib
import subprocess
import multiprocessing

from math import ceil

from setuptools import setup, Extension
from setuptools import find_packages
from setuptools.command.build_ext import build_ext

def get_project_name() -> str:
    return open(pathlib.Path().absolute() / "project_name.txt", "r").read().replace("\n", "")

def get_project_version() -> str:
    aidge_root = pathlib.Path().absolute()
    version = open(aidge_root / "version.txt", "r").read().strip()
    return version

if __name__ == '__main__':

    setup(
        name=get_project_name(),
        version="0.0.1",
        python_requires='>=3.7',
        description=DOCLINES[0],
        long_description_content_type="text/markdown",
        long_description="\n".join(DOCLINES[2:]),
        classifiers=[c for c in CLASSIFIERS.split('\n') if c],
        packages=find_packages(where="."),
        install_requires=['aidge_core'],
    )
